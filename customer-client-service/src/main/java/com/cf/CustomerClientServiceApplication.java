package com.cf;

import com.cf.shared.CustomerServiceFeignClient;
import com.cf.shared.GithubClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackageClasses = {CustomerServiceFeignClient.class, GithubClient.class})
@ComponentScan("com.cf")
public class CustomerClientServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerClientServiceApplication.class, args);
	}
}

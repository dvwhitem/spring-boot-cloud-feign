package com.cf.client;

import com.cf.shared.Customer;
import com.cf.model.MessageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by vitaliy on 12/3/16.
 */
@Component
public class CustomerServiceRestTemplateClient {

    @Autowired
    private RestTemplate restTemplate;

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public MessageWrapper<Customer> getCustomer(int id) {
        Customer customer = restTemplate.exchange(
                "http://customer-service/customer/{id}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Customer>() {
                },
                id
        ).getBody();

        return new MessageWrapper<>(customer, "server called using eureka with rest template");
    }
}

package com.cf.controller;

import com.cf.shared.GithubClient;
import com.cf.shared.GithubUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vitaliy on 12/3/16.
 */
@RestController
public class GithubFeignController {

    private static Logger logger = LoggerFactory.getLogger(CustomerFeignController.class);

    @Autowired
    private GithubClient githubClient;

    @RequestMapping(value = "/github-client-feign/{username}", method = RequestMethod.GET, produces = "application/json")
    public GithubUser getUser(@PathVariable String username) {

        logger.debug("Reading github user feign client with username " + username);

        return  githubClient.getUser(username);
    }
}

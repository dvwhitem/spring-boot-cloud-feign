package com.cf.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by vitaliy on 12/3/16.
 */
@Data
@AllArgsConstructor
public class MessageWrapper<T> {

    private T wrapped;
    private String message;
}

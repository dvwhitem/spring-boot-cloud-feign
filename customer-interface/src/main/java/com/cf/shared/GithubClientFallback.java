package com.cf.shared;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by vitaliy on 12/3/16.
 */
@Component
public class GithubClientFallback implements GithubClient {
    @Override
    public GithubUser getUser(@RequestParam("username") String username) {
        return new GithubUser("default", "", "", "");
    }
}

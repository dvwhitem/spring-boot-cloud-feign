package com.cf.shared;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by vitaliy on 12/3/16.
 */
@FeignClient(value = "github-service", url = "https://api.github.com", fallback = GithubClientFallback.class)
public interface GithubClient {

    @RequestMapping(value = "/users/{username}")
    GithubUser getUser(@RequestParam("username") String username);
}

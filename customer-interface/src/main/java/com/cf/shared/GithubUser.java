package com.cf.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by vitaliy on 12/3/16.
 */
@Data
@AllArgsConstructor
public class GithubUser {

    private String login;
    private String id;
    private String avatar_url;
    private String url;
}
